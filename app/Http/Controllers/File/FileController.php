<?php

namespace App\Http\Controllers\File;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    public function promo(){
        $headers = array(
            'Content-Type: application/pdf',
        );
        return response()->download(public_path('img/promoit01.png'), 'promo_itao.pdf', $headers);
    }    

    public function promoSave(Request $request){
        $fileName = 'promobm01.png';
        $path = $request->file('photo')->move(public_path('/img'),$fileName);
        $photoURL = URL('/'.$fileName);
        return response()->json(['url'=>$photoURL],200);
    }    
}
