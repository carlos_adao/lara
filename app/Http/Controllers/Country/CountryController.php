<?php

namespace App\Http\Controllers\Country;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CountryModel;
use Validator;

class CountryController extends Controller
{   
    //Return a list of all coutries registered in the database
    public function country(){
        return response()->json(CountryModel::get(), 200);
    }
    
    //Return a country according to th id 
    public function countryByID($id){
        $country = CountryModel::find($id);
        if(is_null($country)){
            return response()->json(["messege"=>"Record not found!"], 404);
        }
        return response()->json($country, 200);
    }

    //Saves a country in the database
    public function countrySave(Request $request){
        $rules = [
            'name'=>'required|min:3',
            'iso'=>'required|min:2|max:2'  
        ];
        $validator = Validator::make($request->all(), $rules); 
        if($validator->fails()){
            return response()->json(["messege"=>"Erro ao cadastrar"], 400);
        }

        $country = CountryModel::create($request->all());
        return response()->json($country, 201);
    }

    //Update information for a specific country 
    public function countryUpdate(Request $request, $id){
        $country = CountryModel::find($id);
        if(is_null($country)){
            return response()->json(["messege"=>"Record not found!"], 404);
        }
        $country->update($request->all());
        return response()->json($country, 200);
    }

    //Delete for infomation country of database
    public function countryDelete(Request $request, $id){
        $country = CountryModel::find($id);
        if(is_null($country)){
            return response()->json(["messege"=>"Record not found!"], 404);
        }
        $country->delete();
        return response()->json(null, 204);
    }    
}
