<?php

Auth::routes();

/*Retornando uma view*/
Route::get('/', function () {
    return view('welcome');
})->name('welcome');

/*Regra de acesso para grupo de rotas*/
Route::middleware([])->group(function(){
    Route::prefix('admin')->group(function(){
        Route::get('/', function () {
            return view('welcome');
        })->name('admin');
        
        Route::namespace('Admin')->group(function(){
            Route::get('/produtos', 'AdminController@create')->name('admin.prod');
        });
    });
});

/*Estabelecendo as rotas resource de um crud*/
Route::resource('products','ProductController');

/*Redirecionando uma rota*/
Route::get('/home', 'HomeController@index')->name('home');